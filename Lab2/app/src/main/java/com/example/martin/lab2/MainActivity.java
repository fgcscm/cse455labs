package com.example.martin.lab2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    // declaring view variables
    private EditText mEdit;
    private TextView OutputText;
    private Button copyButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Assign the views from the layout file to the corresponding variables
        mEdit   = (EditText)findViewById(R.id.editText);
        OutputText = (TextView) findViewById(R.id.textView3);
        copyButton = (Button) findViewById(R.id.button);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OutputText.setText(mEdit.getText().toString());
            }
        };
        copyButton.setOnClickListener(listener);
    }


}
